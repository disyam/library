// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

/**
 * @description handler for book-created
 * @param {import('@mocobaas/server-sdk').EventContext} ctx
 */
async function handler(ctx) {
  console.log("book created:");
  console.log(ctx.data);
  await ctx.moco.tables.create({
    table: "book_log",
    data: {
      name: ctx.data.name,
      author: ctx.data.author,
      book_id: ctx.data.id,
    },
  });
}

module.exports = handler;
