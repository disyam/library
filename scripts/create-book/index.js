// @ts-check
/**
 * You can also import another NPM package
 * const _ = require("lodash");
 * const moment = require("moment");
 */

const chance = require("chance").Chance();
const bluebird = require("bluebird");

/**
 * @description handler for create-book
 * @param {import('@mocobaas/server-sdk').Context} ctx
 * @returns {Promise<import('@mocobaas/server-sdk').ReturnCtx>}
 */
async function handler(ctx) {
  const transaction = await ctx.moco.tables.createTransaction();
  const data = await Promise.all([
    ctx.moco.tables.create({
      table: "book",
      data: {
        name: chance.name(),
        author: "author1",
      },
      transaction,
      event: true,
    }),
    ctx.moco.tables.create({
      table: "book",
      data: {
        name: chance.name(),
        author: "author2",
      },
      transaction,
    }),
    ctx.moco.tables.create({
      table: "book",
      data: {
        name: chance.name(),
        author: "author2",
      },
      event: true,
    }),
  ]);
  await bluebird.delay(3000);
  await transaction.commit();
  return { data };
}

module.exports = handler;
